# --- Build stage
FROM docker.io/node:21 AS build-ui
WORKDIR /app
COPY slrp/ui /app

RUN \
  npm install -g typescript && \ 
  npm install && \
  npm run build



# --- Build stage
FROM docker.io/golang:1.20 AS build-backend
WORKDIR /app
COPY slrp /app

COPY --from=build-ui /app/build /app/ui/build
RUN \
  go mod tidy && \
  go mod vendor && \
  go build -ldflags "-s -w" main.go



# --- Final image stage
FROM docker.io/alpine
ENV PWD="/app"
WORKDIR $PWD

RUN apk add --no-cache gcompat

COPY --from=build-backend /app/main /app/slrp

ENV SLRP_APP_STATE="$PWD/.slrp/data"
ENV SLRP_APP_SYNC="1m"
ENV SLRP_LOG_LEVEL="info"
ENV SLRP_LOG_FORMAT="pretty"
ENV SLRP_SERVER_ADDR="127.0.0.1:8089"
ENV SLRP_SERVER_READ_TIMEOUT="15s"
ENV SLRP_MITM_ADDR="127.0.0.1:8090"
ENV SLRP_MITM_READ_TIMEOUT="15s"
ENV SLRP_MITM_IDLE_TIMEOUT="15s"
ENV SLRP_MITM_WRITE_TIMEOUT="15s"
ENV SLRP_CHECKER_TIMEOUT="5s"
ENV SLRP_CHECKER_STRATEGY="simple"
ENV SLRP_HISTORY_LIMIT="10000"
ENV SLRP_DIALER_WIREGUARD_CONFIG_FILE=""

RUN mkdir ./.slrp

EXPOSE 8089 8090

CMD ["./slrp"]
